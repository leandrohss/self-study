package com.luc.project2;

import java.util.ArrayList;
import java.util.List;

public class Family implements Beneficiary {

	List<Beneficiary> beneficiaries;
	
	public Family() {
		beneficiaries = new ArrayList<>();
	} 
	
	@Override
	public void receiveMoney() {
		for (Beneficiary b : beneficiaries)
			b.receiveMoney();
	}
	
	public void addBeneficiary(Beneficiary beneficiary) {
		beneficiaries.add(beneficiary);
	}

	public void removeBeneficiary(Beneficiary beneficiary) {
		beneficiaries.remove(beneficiary);
	}
	
}
