package com.luc.project2;

public class Person implements Beneficiary {

	private String name;
	private double money;
	
	public Person(String name) {
		this.name = name;
	}
	
	@Override
	public void receiveMoney() {
		System.out.println(name + " received money");
		money += 50;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}
	
}
