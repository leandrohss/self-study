package com.luc.project2;

public class Main {

	public static void main(String[] args) {
		new Main();
	}
	
	public Main() {

		// Individual beneficiaries
		Beneficiary person1 = new Person("Joe");
		Beneficiary person2 = new Person("Leandro");
		Beneficiary person3 = new Person("Zewdie");
		
		// People in a family within a composite
		Family family = new Family();
		Beneficiary familyMember1 = new Person("George");
		Beneficiary familyMember2 = new Person("Future");
		Beneficiary familyMember3 = new Person("Wesley Safadao");
		
		family.addBeneficiary(familyMember1);
		family.addBeneficiary(familyMember2);
		family.addBeneficiary(familyMember3);
		
		// A bigger family composed of persons and a smaller family
		Family bigFamily = new Family();
		bigFamily.addBeneficiary(family);
		bigFamily.addBeneficiary(person1);
		bigFamily.addBeneficiary(person2);
		bigFamily.addBeneficiary(person3);
		
		// All members within the big family receive money
		bigFamily.receiveMoney();

	}
	
}
